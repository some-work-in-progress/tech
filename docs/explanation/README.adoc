= About Explanatory documentation
Duncan Hutty <dhutty@allgoodbits.org>
:created: 2023-09-24
:modified: 2023-09-24 15:29
:swip-domain: someworkinprogress.org
:swip-display-name: Some Work in Progress
:swip-proj: some-work-in-progress
:url-gitlab: https://gitlab.com/{swip-proj}
:url-swip: https://{swip-domain}
:icons: font
:toc: left
:leveloffset: +1

This directory SHOULD contain material that is oriented towards providing _understanding_. Documentation about decision-making and implications:

* Why do we do it this way?
* What are the consequences?
