#!/bin/bash

# This script:
#   - Takes exactly two parameters: "<short_hostname>. <domain_name>."; yes, both the '.' are required.
#   - needs AWS credentials with privileges to access Route53 for the specified Hosted Zone
#   - updates the right hand side of an A record with the IPv4 address that checkip.amazonaws.com sees as the source of the request which is (intended) to be the public IPv4 Address of the NATted machine running this script
#   - should be executed as "$0 <short_hostname>. <domain_name>." (yes, both the '.' are required.

function die {
    echo "$@"
    exit 1
}

command -v curl || die "curl must be installed"
command -v jq || die "jq must be installed"
[[ -n $1 && -n $2 ]] || die "Takes exactly two parameters: \"$0 <short_hostname>. <domain_name>.\"; yes, both the '.' are required"

SHORT_HOSTNAME="$1"
DNS_DOMAIN="$2"
HTTP=$(command -v curl)

FQDN="${SHORT_HOSTNAME}${DNS_DOMAIN}"
TYPE=${TYPE:-A}
MY_IP=$($HTTP checkip.amazonaws.com)

# Get the Route53 Hosted Zone ID for the domain
HOSTED_ZONE_ID=$(aws route53 list-hosted-zones \
    | jq -r '.HostedZones[]|select(.Name=="'"$DNS_DOMAIN"'").Id' \
    | sed -e 's!^.*/!!'
    )
# Get the first value of the A record for the specified FQDN from this HostedZone
RECORDED_IP=$(aws route53 list-resource-record-sets --hosted-zone-id "${HOSTED_ZONE_ID}" |
    jq -r '.ResourceRecordSets[] | select (.Name == "'"$FQDN"'") | select (.Type == "'"$TYPE"'") | .ResourceRecords[0].Value'
)

[[ "$MY_IP" == "$RECORDED_IP" ]] && echo "No update required" && exit 0

tempfoo=$(basename "$0")
TMPFILE=$(mktemp /tmp/"${tempfoo}".XXXXXX) || exit 1
COMMENT="Updated by $(hostname) at $(date +%FT%T)"

# Use jq to construct the ChangeRequest JSON as required by AWS Route53
jq -r --null-input \
    --arg HOSTED_ZONE_ID "${HOSTED_ZONE_ID}" \
    --arg MY_IP "${MY_IP}" \
    --arg FQDN "${FQDN}" \
    --arg TYPE "${TYPE}" \
    --arg COMMENT "${COMMENT}" \
'{"HostedZoneId": $HOSTED_ZONE_ID, "ChangeBatch": { "Comment": $COMMENT, "Changes":[ { "Action":"UPSERT", "ResourceRecordSet": { "ResourceRecords":[ { "Value":$MY_IP } ], "Name":$FQDN, "Type":$TYPE, "TTL":300 } } ] }}' >> "${TMPFILE}"

# Call the AWS Route53 API via the AWS CLI
aws route53 change-resource-record-sets \
    --hosted-zone-id "${HOSTED_ZONE_ID}" \
    --cli-input-json "file://${TMPFILE}"

rm "${TMPFILE}"
