# Documentation at https://gitlab.com/some-work-in-progress/tech/-/tree/main/docs
ControlPath ~/.ssh/cp-tunnel@%h:%p
ControlMaster auto
ControlPersist 2h

Host ingress_tunnel
    Compression yes
    User <whoever>
    IdentityFile <path to private key file>
    Hostname <hostname>.someworkinprogress.org
    Port <port that is forwarded to the REMOTE_HOST>
    # When doing Remote Port forwards: without ExitOnForwardFailure, if autossh detects the dropped connection before the remote end notices (likely) the subsequent reconnect will be unable to bind to your specified remote port because it’s still bound to the stale SSH session. So the session will be up, but the reason for the session (the port forward) will be unavailable. With this extra option SSH immediately quits if that bind fails and autossh will just retry until it succeeds (after the remote end detects the stale session).
    ExitOnForwardFailure=yes
    # Sets a timeout interval in seconds after which if no data has been received from the server, ssh(1) will send a message through the encrypted channel to request a response from the server.
    ServerAliveInterval 15
    # Sets the number of server alive messages (see below) which may be sent without ssh(1) receiving any messages back from the server. If this threshold is reached while server alive messages are being sent, ssh will disconnect from the server, terminating the session.
    ServerAliveCountMax 6
    # We use ssh protocol level keepalives instead
    TCPKeepAlive no

    # typically specified by the systemd service's ExecStart command for autossh
    # RemoteForward  2022 localhost:22


# vi: ft=sshconfig
