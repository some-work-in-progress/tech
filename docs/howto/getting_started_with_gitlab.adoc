= Getting started with GitLab
Duncan Hutty <dhutty@allgoodbits.org>
:created: 2023-09-24
:modified: 2023-09-24 18:31
:swip-domain: someworkinprogress.org
:swip-display-name: Some Work in Progress
:swip-proj: some-work-in-progress
:url-gitlab: https://gitlab.com/{swip-proj}
:url-swip: https://{swip-domain}
:icons: font
:toc: left
:leveloffset: +1

* Git is a distributed version control system, designed to keep track of changes to files/documents.
*  link:https://gitlab.com[GitLab] is a collaboration platform that provides much more functionality, mostly for software developers.

{swip-display-name} uses the GitLab collaboration platform to share and manage documents, code, and more.

. Sign up for an account with gitlab.com if you don't already have one.
. Give your GitLab username to one of the admins who grant permissions to one or more of the SWiP projects.
. If you are unfamiliar with git/gitlab, there are lots of tutorials on the web, including at https://docs.gitlab.com/ee/tutorials

== Security

* Please add link:https://docs.gitlab.com/ee/user/profile/account/two_factor_authentication.html[Multi-Factor Authentication(MFA)] to secure your account.
* If you are going to only the GitLab Web IDE (i.e. in your web browser) to edit files, you can stop there.
* If you plan to edit files locally, on your own computer, you should set up SSH-based access, perhaps using GitLab's own link:https://docs.gitlab.com/ee/user/ssh.html[ssh documentation].
