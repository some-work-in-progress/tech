variable "domain" {
  type        = string
  description = "The main domain, under which subdomains/records will be managed"
  default     = "someworkinprogress.org"
}

variable "default_ttl" {
  type        = number
  description = "Number of seconds for the TTL default."
  default     = 600
}

variable "tags" {
  type        = map(string)
  default     = {}
  description = "Tags to apply"
}
