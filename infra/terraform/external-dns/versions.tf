terraform {
  required_version = ">= 1.5,< 1.6"

  required_providers {
    aws = {
      source  = "aws"
      version = "~> 5"
    }
  }
}

provider "aws" {
  region = "us-east-2"
}
