data "gitlab_current_user" "me" {
}

locals {
  created_groups = merge(module.gitlab_group_module_owners.created_groups, module.gitlab_groups.created_groups, {})
  directory      = yamldecode(file("./SWiP-directory.yaml"))
  users          = local.directory.users
  groups         = local.directory.groups

  group_defaults = merge(
    {
      path      = var.top_level_org.name
      parent_id = module.gitlab_group_module_owners.created_groups["swip-owners"].id
    },
    var.group_defaults
  )

  project_defaults = merge(
    {
      # some_project_var = some_default,
    },
    var.project_defaults
  )

  projects = { for project_name, project in var.projects : project_name => merge(local.project_defaults, { description = lookup(project, "description", "The SWiP project: ${project_name}") }, project) }

  # This calculates a list of group _names_: one of each of the var.default_groups for each project
  group_names         = [for g in setproduct(keys(var.projects), var.default_subgroups) : join("-", g)]
  group_privilege_map = { admins = "maintainer", contributors = "developer" }
}

module "gitlab_group_module_owners" {
  source = "../../terraform_modules/upstream/terraform-gitlab-gitlab//modules/gitlab-group-module"
  groups = {
    swip-owners = merge(var.group_defaults,
      {
        name                    = "Some Work in Progress"
        path                    = var.top_level_org.name
        parent_id               = 0
        request_access_enabled  = true
        subgroup_creation_level = "maintainer"
        visibility_level        = "public"
      }
    )
  }
}

data "gitlab_user" "this" {
  for_each = local.users
  username = each.key
}

data "gitlab_group" "this" {
  for_each  = local.groups
  full_path = join("/", [var.top_level_org.name, each.key])
}

resource "gitlab_group_membership" "SWiP_gitlab_io-contributors" {
  count        = length(local.groups["${var.top_level_org.name}.gitlab.io-contributors"])
  user_id      = data.gitlab_user.this[local.groups["${var.top_level_org.name}.gitlab.io-contributors"][count.index]].id
  group_id     = data.gitlab_group.this["${var.top_level_org.name}.gitlab.io-contributors"].id
  access_level = local.group_privilege_map["contributors"]
}

resource "gitlab_group_membership" "tech-contributors" {
  count        = length(local.groups["tech-contributors"])
  user_id      = data.gitlab_user.this[local.groups["tech-contributors"][count.index]].id
  group_id     = data.gitlab_group.this["tech-contributors"].id
  access_level = local.group_privilege_map["contributors"]
}

module "gitlab_groups" {
  source = "../../terraform_modules/upstream/terraform-gitlab-gitlab//modules/gitlab-group-module"
  groups = { for group in local.group_names : group => merge(local.group_defaults, { path = group }, lookup(var.groups, group, { name = group }), try(regex("admin|owner", group), null) != null ? { require_two_factor_authentication = true } : {}) }
}

module "gitlab_project_module" {
  source   = "../../terraform_modules/upstream/terraform-gitlab-gitlab//modules/gitlab-project-module"
  projects = { for project_name, project in local.projects : project_name => merge({ name = project_name, namespace_id = module.gitlab_group_module_owners.created_groups["swip-owners"].id }, project) }
}

resource "gitlab_project_share_group" "admins" {
  for_each     = local.projects
  group_id     = local.created_groups["${each.key}-admins"].id
  group_access = "maintainer"
  project      = module.gitlab_project_module.created_projects[each.key].id
}

resource "gitlab_project_share_group" "contributors" {
  for_each     = local.projects
  group_id     = local.created_groups["${each.key}-contributors"].id
  group_access = "developer"
  project      = module.gitlab_project_module.created_projects[each.key].id
}

resource "gitlab_project_share_group" "guest" {
  for_each     = local.projects
  group_id     = local.created_groups["${each.key}-guest"].id
  group_access = "guest"
  project      = module.gitlab_project_module.created_projects[each.key].id
}

# Use something like this once some-work-in-progress/tech has more admin users than var.owner
# resource "gitlab_group_membership" "tech-admins" {
#   count        = try(local.groups["tech-admins"], 0) == 0 ? length(local.groups["tech-admins"]) : 0
#   user_id      = data.gitlab_user.this[local.groups["tech-admins"][count.index]].id
#   group_id     = data.gitlab_group.this["tech-admins"].id
#   access_level = local.group_privilege_map["admins"]
# }
