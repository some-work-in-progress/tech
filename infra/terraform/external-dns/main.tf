locals {
  yaml_zone = yamldecode(file("./zone.yaml"))
}

import {
  to = aws_route53_zone.this["someworkinprogress.org"]
  id = "Z00745881MAD26NFZTEGR"
}

resource "aws_route53_zone" "this" {
  for_each = { for k, v in local.yaml_zone : v.zone => k... }
  name     = each.key

  tags = var.tags
}

resource "aws_route53_record" "this" {
  for_each = local.yaml_zone
  zone_id  = aws_route53_zone.this[each.value.zone].zone_id
  name     = each.key

  ttl     = lookup(each.value, "ttl", var.default_ttl)
  type    = each.value.type
  records = flatten([each.value.rhs])
}

resource "aws_route53domains_registered_domain" "SWiP" {
  domain_name = var.domain
}

resource "aws_route53_record" "dmarc-SWiP" {
  zone_id = aws_route53_zone.this[var.domain].zone_id
  name    = "_dmarc.someworkinprogress.org"
  ttl     = var.default_ttl
  type    = "TXT"
  records = ["v=DMARC1; p=none; rua=mailto:postmaster@someworkinprogress.org; ruf=mailto:postmaster@someworkinprogress.org"]
}

resource "aws_route53_record" "SPF-SWiP" {
  zone_id = aws_route53_zone.this[var.domain].zone_id
  name    = "someworkinprogress.org"
  ttl     = var.default_ttl
  type    = "TXT"
  records = ["v=spf1 a:mail.someworkinprogress.org mx -all ra=postmaster"]
}

resource "aws_route53_record" "SPF-mail-SWiP" {
  zone_id = aws_route53_zone.this[var.domain].zone_id
  name    = "mail.someworkinprogress.org"
  ttl     = var.default_ttl
  type    = "TXT"
  records = ["v=spf1 a -all ra=postmaster"]
}

resource "aws_route53_record" "MX-SWiP" {
  zone_id = aws_route53_zone.this[var.domain].zone_id
  name    = ""
  ttl     = var.default_ttl
  type    = "MX"
  records = ["10 mail.someworkinprogress.org"]
}

resource "aws_route53_record" "DKIM-SWiP" {
  zone_id = aws_route53_zone.this[var.domain].zone_id
  name    = "stalwart._domainkey.someworkinprogress.org."
  ttl     = var.default_ttl
  type    = "TXT"
  # The \"\" below is to accommodate the 255 character limit
  records = ["v=DKIM1; k=rsa; p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmg0UwtB2wc7LGPxWSMQNZa6O3BiSj8qYZSYyWZdGpGuohDNEzaqxke4a27YIdaYb0giiMWycH9ZGuPsC19V6v9UATSwY3JptgbHlmh3HhD1AQa7Q+J3J9Nf7WWCJOR4aZeJ+R3JdskDT0IWPOWingkCbPOESCSITCBxkObvx4Mr6\"\"m8PHCDu8P9GuwlB0F/+KVh+q1ifpAh9zoTN1V7J9HMoUh/s3ARE8FMRlySAnfrGVMXvByRmVhr3julYndJcfVCdUbePJ1hpxUK6NSsC+lD90EmopMmWfelTy0LJ2vDKnUsGrFgQTKqKoNpjTHZNzcli/LOM629kkc6ufW6CyjQIDAQAB"]
}


resource "aws_route53_record" "gitlab_pages_verification" {
  zone_id = aws_route53_zone.this[var.domain].zone_id
  name    = "_gitlab-pages-verification-code.${local.yaml_zone["www.${var.domain}"].rhs}"
  ttl     = var.default_ttl
  type    = "TXT"
  records = ["gitlab-pages-verification-code=99e61bbf4b871fada07088f1ad8dffa2"]
}
