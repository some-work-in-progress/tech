= Contributing
Duncan Hutty <dhutty@allgoodbits.org>
:created: 2023-09-25
:modified: 2023-09-25 23:04
:swip-domain: someworkinprogress.org
:swip-display-name: Some Work in Progress
:url-swip: https://{swip-domain}
:swip-org: some-work-in-progress
:swip-proj: {swip-org}/tech
:url-gitlab: https://gitlab.com/{swip-proj}
:url-issues: {url-gitlab}/-/issues
:icons: font
:toc: left
:leveloffset: +1

We welcome feedback on and help with this project. By participating in this project, you agree to abide by the link:./CODE_OF_CONDUCT.adoc[Code of Conduct]. Please take a few moments to read it.

We are particularly looking for help with our open {url-issues}[Issues] - creating them, specifying them, and implementing them. All of this helps make progress. We appreciate all forms of contribution - not just code - that can include documentation, clarifications, typo corrections and much more.

== Getting started

* Identify some {url-issues}[Issue] that you would like to work on or if there is no existing Issue relevant to your interest, create a new one.
* Join the conversation.

== Git/GitLab

See link:./docs/howto/getting_started_with_gitlab.adoc[Getting Started with GitLab]
