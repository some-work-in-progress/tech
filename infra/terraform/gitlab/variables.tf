variable "gitlab_token" {}
variable "owner" {
  type        = string
  default     = "dhutty"
  description = "The GitLab username of the account that will Own everything created by this module."
}

variable "top_level_org" {
  type = map(string)
  default = {
    id           = "74321140"
    name         = "some-work-in-progress"
    email        = "mistymountain.swip@gmail.com"
    domain       = "someworkinprogress.org"
    display_name = "Some Work in Progress"
    description  = ""
  }
}

variable "project_defaults" {
  default = {
    initialize_with_readme          = true
    issues_enabled                  = true
    issues_access_level             = "private"
    wiki_enabled                    = false
    container_registry_access_level = "private"
    environments_access_level       = "private"
    infrastructure_access_level     = "private"
    monitor_access_level            = "private"
    release_access_level            = "private"
    prevent_secrets                 = true
    visibility_level                = "public"
  }
  description = "See https://github.com/EvoltDev/terraform-gitlab-gitlab/blob/v1.0.1/modules/gitlab-project-module/main.tf for upstream defaults."
}

variable "group_defaults" {
  default = {
    lfs_enabled         = true
    auto_devops_enabled = false
    # https://docs.gitlab.com/ee/api/groups.html#options-for-default_branch_protection
    # Users with the Maintainer role can:
    #    - Push new commits
    #    - Force push changes
    #    - Accept merge requests
    # Users with the Developer role can:
    #    - Accept merge requests
    default_branch_protection = 3
    project_creation_level    = "maintainer" # privilege levels _below_ this are not allowed to create projects.
    subgroup_creation_level   = "owner"      # privilege levels _below_ this are not allowed to create subgroups.
    visibility_level          = "public"
  }
}

variable "default_subgroups" {
  type        = set(string)
  default     = ["admins", "contributors", "guest"]
  description = "A list of group names that will be created by default for each project"
}

variable "projects" {
  default = {
    "tech"                            = {}
    "some-work-in-progress.gitlab.io" = {}
  }
}

variable "groups" {
  default = {
    tech_readonly = {
      name = "tech-readonly"
    }
    tech_admins = {
      name = "tech-admins"
    }
    tech_contributors = {
      name = "tech-contributors"
    }
  }
}
