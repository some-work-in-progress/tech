terraform {
  required_version = ">= 1.5,< 1.6"

  required_providers {
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "~> 16.4"
    }
  }
}

provider "gitlab" {
  token = var.gitlab_token
}
