output "groups" {
  value       = local.group_names
  description = "The list of GitLab Groups."
}

output "projects" {
  value       = local.projects
  description = "The list of GitLab Projects."
  sensitive   = true
}
