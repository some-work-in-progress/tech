data "gitlab_project" "SWiP_www" {
  path_with_namespace = join("/", [var.top_level_org.name, "some-work-in-progress.gitlab.io"])
}

resource "gitlab_pages_domain" "SWiP_www" {
  project          = data.gitlab_project.SWiP_www.id
  domain           = join(".", ["www", var.top_level_org.domain])
  auto_ssl_enabled = true
}

## DNS probably needs the following:
# CNAME: www.someworkinprogress.org => some-work-in-progress.gitlab.io
# TXT: _gitlab-pages-verification-code.some-work-in-progress.gitlab.io => gitlab-pages-verification-code=data.gitlab_pages_domain.SWiP_www.verification_code
## See Also:
# https://docs.gitlab.com/ee/user/project/pages/getting_started_part_one.html
# https://docs.gitlab.com/ee/user/project/pages/custom_domains_ssl_tls_certification/index.html
